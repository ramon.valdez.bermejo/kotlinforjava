package rationals

import java.lang.IllegalArgumentException
import java.math.BigInteger

data class Rational(var numerator: BigInteger, var denominator: BigInteger = BigInteger.ONE): Comparable<Rational> {

    init {
        if (denominator == BigInteger.ZERO) {
            throw IllegalArgumentException("IllegalArgumentException")
        }
        val greatestCommonDivisor = numerator.gcd(denominator)

        this.numerator = numerator / greatestCommonDivisor
        this.denominator = denominator / greatestCommonDivisor

        if (this.denominator < BigInteger.ZERO) {
            this.numerator *= -BigInteger.ONE
            this.denominator *= -BigInteger.ONE
        }
    }

    override operator fun compareTo(other: Rational): Int {
        return (numerator * other.denominator).compareTo(other.numerator * denominator)
    }

    override fun toString(): String {
        if (denominator == 1.toBigInteger()) {
            return numerator.toString()
        } else {
            val minimumCommonMultiple = numerator.gcd(denominator)
            if (denominator / minimumCommonMultiple == 1.toBigInteger())
                return (numerator / minimumCommonMultiple).toString()
            else {
                if (denominator < BigInteger.ZERO) {
                    return (numerator * -BigInteger.ONE / minimumCommonMultiple).toString() + "/" + (denominator * -BigInteger.ONE / minimumCommonMultiple).toString()
                } else
                    return (numerator / minimumCommonMultiple).toString() + "/" + (denominator / minimumCommonMultiple).toString()
            }

        }
    }
}

    fun main() {
        val half = 1 divBy 2
        val third = 1 divBy 3

        val sum: Rational = half + third
        println(5 divBy 6 == sum)

        val difference: Rational = half - third
        println(1 divBy 6 == difference)

        val product: Rational = half * third
        println(1 divBy 6 == product)

        val quotient: Rational = half / third
        println(3 divBy 2 == quotient)

        val negation: Rational = -half
        println(-1 divBy 2 == negation)

        println((2 divBy 1).toString() == "2")
        println((-2 divBy 4).toString() == "-1/2")
        println("117/1098".toRational().toString() == "13/122")

        val twoThirds = 2 divBy 3
        println(half < twoThirds)

        println(half in third..twoThirds)

        println("912016490186296920119201192141970416029".toBigInteger() divBy
                "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)
    }

infix fun BigInteger.divBy(denominator: BigInteger): Rational {
    if (denominator == BigInteger.ZERO)
        throw IllegalArgumentException("IllegalArgumentException")
    else
        return Rational(this, denominator)

}

infix fun Long.divBy(denominator: Long): Rational {
    return Rational(BigInteger.valueOf(this), BigInteger.valueOf(denominator))
}

infix fun Int.divBy(denominator: Int): Rational {
    if (denominator == 0)
        throw IllegalArgumentException("IllegalArgumentException")
    else
        return Rational(BigInteger.valueOf(this.toLong()), BigInteger.valueOf(denominator.toLong()))
}

fun String.toRational(): Rational {
    if (this.isNullOrBlank())
        throw IllegalArgumentException("IllegalArgumentException")
    else {
        var rationalParts: List<String>
        var num: BigInteger
        var den: BigInteger
        if (this.contains('/')) {
            rationalParts = this.split("/")
            num = rationalParts.get(0).toBigInteger()
            den = rationalParts.get(1).toBigInteger()
        } else {
            num = this.toBigInteger()
            den = BigInteger.ONE
        }

        return Rational(num, den)
    }
}

operator fun Rational.plus(other: Rational): Rational {
    return Rational( (numerator * other.denominator) + (other.numerator * denominator), denominator * other.denominator)
}

operator fun Rational.minus(other: Rational): Rational {
    return Rational( (numerator * other.denominator) - (other.numerator * denominator), denominator * other.denominator)
}

operator fun Rational.times(other: Rational): Rational {
    return Rational(numerator * other.numerator, denominator * other.denominator)
}

operator fun Rational.div(other: Rational): Rational {
    return Rational(numerator * other.denominator, denominator * other.numerator)
}

//operator fun Rational.unaryMinus() = Rational(-this.numerator, this.denominator)
operator fun Rational.unaryMinus(): Rational {
    return -numerator divBy denominator
}





