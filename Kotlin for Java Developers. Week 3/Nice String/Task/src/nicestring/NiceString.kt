package nicestring

fun String.isNice(): Boolean {
    var accomplishedConditions = 0
    var previousChar: Char = ' '
    var doubleLetter = false
    val vowels = listOf('a','e','i','o','u')
    //val str = "whattever"
    val strList: List<Char> = this.toList()

    //Check first condition
    var containsSubstrings = this.contains("bu") ||
                             this.contains("ba") ||
                             this.contains("be")

    if (!containsSubstrings) accomplishedConditions++

    //Check second condition
    val filteredVowels = strList.filter { vowels.contains(it) }
    if (filteredVowels.size >= 3) accomplishedConditions++

    //Check third condition
    strList.forEach {
        if (it == previousChar) {
            if (doubleLetter != true) {
                doubleLetter = true
            }
        }
        previousChar = it
    }
    if (doubleLetter) accomplishedConditions++

    return accomplishedConditions >= 2
}