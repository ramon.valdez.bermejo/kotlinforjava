package board

import board.Direction.*

fun createSquareBoard(width: Int): SquareBoard = SquareBoardImpl(width)
fun <T> createGameBoard(width: Int): GameBoard<T> = GameBoardImpl(width)

open class SquareBoardImpl(override var width: Int) : SquareBoard {

    private val board = (1..width).flatMap { i -> (1..width).map { Cell(i, it) } }

    override fun getCellOrNull(i: Int, j: Int): Cell? =
            board.filter { it.i == i }.filter { it.j == j }.firstOrNull()

    override fun getCell(i: Int, j: Int): Cell = getCellOrNull(i,j)!!

    override fun getAllCells(): Collection<Cell> = board

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> {
        val row = board.filter { it.i == i }
        return jRange.takeWhile { it <= width }.map { step -> row.find { cell -> cell.j == step  } as Cell }
    }

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> {
        val col = board.filter { it.j == j }
        return iRange.takeWhile { it <= width }.map { step -> col.find { cell -> cell.i == step  } as Cell }
    }

    override fun Cell.getNeighbour(direction: Direction): Cell? =
            when(direction) {
                DOWN -> getCellOrNull(i+1, j)
                UP -> getCellOrNull(i-1, j)
                LEFT -> getCellOrNull(i, j-1)
                RIGHT -> getCellOrNull(i, j+1)
            }
}

class GameBoardImpl<T>(width: Int) : SquareBoardImpl(width), GameBoard<T> {

    val mutableMap: MutableMap<Cell, T?> = hashMapOf()

    init {
        getAllCells().forEach{ set(it, null) }
    }

    override operator fun get(cell: Cell): T? = mutableMap[cell]

    override operator fun set(cell: Cell, value: T?) {
        mutableMap[cell] = value
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> {
        return mutableMap.filter { predicate(it.value) }.keys
    }

    override fun find(predicate: (T?) -> Boolean): Cell? = filter(predicate).firstOrNull()

    override fun any(predicate: (T?) -> Boolean): Boolean = !filter(predicate).isEmpty()

    override fun all(predicate: (T?) -> Boolean): Boolean = filter(predicate).size == mutableMap.size
}


