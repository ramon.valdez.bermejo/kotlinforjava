package games.game2048

import board.Cell
import board.GameBoard
import kotlin.random.Random
import java.util.*

interface Game2048Initializer<T> {
    /*
     * Specifies the cell and the value that should be added to this cell.
     */
    fun nextValue(board: GameBoard<T?>): Pair<Cell, T>?
}

object RandomGame2048Initializer: Game2048Initializer<Int> {
    private val randomValue = Random(4)
    private fun generateRandomStartValue(): Int =
            if (Random.nextInt(10) == 9) 4 else 2

    private fun getCellRandomly(board: GameBoard<Int?>): Cell? {

        val emptyCells = board.getAllCells()
                .filter { board[it] == null }

        if (emptyCells.isEmpty()) {
            return null
        }
        val cell = emptyCells.shuffled(randomValue).first()
        return cell
    }

    /*
     * Generate a random value and a random cell among free cells
     * that given value should be added to.
     * The value should be 2 for 90% cases, and 4 for the rest of the cases.
     * Use the 'generateRandomStartValue' function above.
     * If the board is full return null.
     */
    override fun nextValue(board: GameBoard<Int?>): Pair<Cell, Int>? {
        val randomCell = getCellRandomly(board)
        val randomInt = generateRandomStartValue()

        var pair: Pair<Cell, Int>? = null
        if (randomCell != null) {
            pair = Pair(randomCell, randomInt)
        }
        return pair
    }
}