package games.gameOfFifteen

/*
 * This function should return the parity of the permutation.
 * true - the permutation is even
 * false - the permutation is odd
 * https://en.wikipedia.org/wiki/Parity_of_a_permutation

 * If the game of fifteen is started with the wrong parity, you can't get the correct result
 *   (numbers sorted in the right order, empty cell at last).
 * Thus the initial permutation should be correct.
 */

lateinit var visitedCells: BooleanArray

lateinit var mapping: IntArray

fun isEven(permutation: List<Int>): Boolean {
    var swapped = 0

    for (i in permutation.indices) {
        for (j in i + 1 until permutation.size) {
            if (isSwapped(permutation, i, j)) {
                swapped++
            }
        }
    }
    return swapped % 2 == 0
}

private fun isSwapped(permutation: List<Int>, i: Int, j: Int): Boolean {
    return i < j && permutation[i] > permutation[j]
}

fun isAdditionalEven(permutation: List<Int>): Boolean {
    visitedCells = BooleanArray(permutation.size + 1)
    mapping = IntArray(permutation.size + 1)

    val transpositions = transpositions(permutation)
    return transpositions % 2 == 0
}

fun transpositions(permutation: List<Int>): Int {
    for (i in 1..permutation.size) {
        visitedCells[i] = false
    }

    for (i in 0 until permutation.size) {
        mapping[permutation[i]] = i + 1
    }

    var transpositionsCount = 0

    for (i in 1..permutation.size) {
        if (!visitedCells[i]) {

            val cycleCalculation = calculation(i)
            transpositionsCount += cycleCalculation - 1
        }
    }
    return transpositionsCount
}

fun calculation(i: Int): Int {
    if (visitedCells[i]) {
        return 0
    }

    visitedCells[i] = true
    val x = calculation(mapping[i])

    return x + 1
}