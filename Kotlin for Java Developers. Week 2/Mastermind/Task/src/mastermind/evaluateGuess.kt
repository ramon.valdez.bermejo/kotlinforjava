package mastermind

data class GuessOccurrence(val guessElement: String, val guessPosition: Int, val secretPosition: Int)

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret: String, guess: String): Evaluation {
    var secretString = StringBuilder(secret)
    var guessString = StringBuilder(guess)
    val secretChunked = secret.chunked(1)
    val guessChunked = guess.chunked(1)
    var listPositions: MutableList<Int> = mutableListOf()
    var positions = 0
    var letters = 0
    var letterPos = 0
    var secretPos = 0
    var guessPos = 0
    var index = 0
    var guessChar: String
    var secretChar: String
    val underScore = "_"

    for (ch in guessChunked) {
        secretChar = secretChunked.get(index)
        if (secretChar.equals(ch)) {
            listPositions.add(index)
            secretPos = index - positions
            secretString.replace(secretPos, secretPos+1, "")
            guessString.replace(secretPos, secretPos+1, "")
            positions++
        }
        index++
    }

    index = 0
    for (ch in guessString) {
        if (secretString.contains(ch)) {
            secretPos = secretString.indexOf(ch)
            secretString = secretString.replace(secretPos, secretPos+1, underScore)
            letters++
        }
        index++
    }

    return Evaluation(listPositions.size, letters)
}
